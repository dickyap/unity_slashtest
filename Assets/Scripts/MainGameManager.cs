﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Slash.Pong.General;
using Slash.Pong.Level;
using TMPro;
using System.Linq;

namespace Slash.Pong.MainGame
{

    public class MainGameManager : MonoBehaviour
    {
        public static MainGameManager init;

        [SerializeField] GameLevel _currentLevel;
        [Space(10)]
        [SerializeField] TMP_Text _txtScore;
        [SerializeField] Ball _ball;

        float _delayDefault = 8;
        int _currentScore = 0;
        bool _isGamePlaying;

        List<float> _spawnPos;

        void Awake() => init = this;
        void Start() => InitializeLevel();
        void InitializeLevel()
        {
            float widthCanvas = ObjectPoolManager.init.GetParentCanvasRTObj().rect.width;
            _spawnPos = new List<float> { -widthCanvas / 3, 0, widthCanvas / 3 };

            _ball.SetBall(_currentLevel);
            _ball.enabled = _isGamePlaying = true;
            StartCoroutine(SpawnEnemy());
        }

        IEnumerator SpawnEnemy()
        {
            while (_isGamePlaying)
            {
                int randTotSpawn = Random.Range(1, _spawnPos.Count);
                System.Random rnd = new System.Random();
                _spawnPos = _spawnPos.OrderBy(x => rnd.Next(_spawnPos.Count)).ToList();

                for (int i = 0; i < randTotSpawn; i++)
                {
                    GameObject temp = ObjectPoolManager.init.GetObjectToSpawn(ConfigGeneral.OBJECTID_ENEMY);
                    temp.GetComponent<RectTransform>().anchoredPosition = new Vector2(_spawnPos[i], 0);
                    temp.GetComponent<Enemy>().SetEnemy(_currentLevel);
                    temp.SetActive(true);
                }

                yield return new WaitForSeconds(_delayDefault / _currentLevel.enemySpeed);
            }
        }

        public ConfigGeneral.ScreenSize GetScreenSpace(Canvas parentCanvas, float objWidth)
        {
            Vector2 _canvasSizeHalf = new Vector2((Screen.width / parentCanvas.scaleFactor), (Screen.height / parentCanvas.scaleFactor)) / 2;
            return new ConfigGeneral.ScreenSize(objWidth - _canvasSizeHalf.x, _canvasSizeHalf.x - objWidth, objWidth - _canvasSizeHalf.y, _canvasSizeHalf.y - objWidth);
        }

        public void UpdateScore() => UpdateScore(_currentLevel.scorePerHit);
        public void UpdateScore(int addScore)
        {
            _currentScore += addScore;
            _txtScore.text = string.Format("Score: {0}", _currentScore);
        }
    }
}