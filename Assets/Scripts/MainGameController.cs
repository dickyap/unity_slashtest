﻿using System.Collections;
using UnityEngine;

namespace Slash.Pong.MainGame
{
    public class MainGameController : MonoBehaviour
    {
        [SerializeField] BoxSlider _boxSlider;
        float _moveValue;

#if UNITY_EDITOR
        void Update()
        {
            if (Input.GetKey(KeyCode.A)) _boxSlider.MoveBoxSlider(-1);
            if (Input.GetKey(KeyCode.D)) _boxSlider.MoveBoxSlider(1);
        }
#endif

        #region Player
        public void MoveBoxSlider(float value) => StartCoroutine(MoveBoxSliderRoutine(_moveValue = value));
        IEnumerator MoveBoxSliderRoutine(float value)
        {
            while (_moveValue != 0)
            {
                _boxSlider.MoveBoxSlider(_moveValue);
                yield return null;
            }
        }
        #endregion
    }

}