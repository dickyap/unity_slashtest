﻿namespace Slash.Pong.General
{
    public static class ConfigGeneral
    {
        public const string OBJECTID_ENEMY = "enemy", OBJECTID_PARTICLE_SMOKE = "smoke", 
            TAG_PLAYER = "Player", TAG_ENEMY = "Enemy", ANIM_TTRIGGER_BOUNCE = "bounce";
        public struct ScreenSize
        {
            public float xMin, xMax, yMin, yMax;
            public ScreenSize(float xMinVal, float xMaxVal, float yMinVal, float yMaxVal)
            {
                xMin = xMinVal; xMax = xMaxVal; yMin = yMinVal; yMax = yMaxVal;
            }

            public ScreenSize(float xMinVal, float xMaxVal)
            {
                xMin = xMinVal; xMax = xMaxVal; yMin = 0; yMax = 0;
            }
        }

    }
}