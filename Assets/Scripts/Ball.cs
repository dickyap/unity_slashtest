﻿using UnityEngine;
using Slash.Pong.General;

namespace Slash.Pong.MainGame
{
    public class Ball : MonoBehaviour
    {
        float _speed;
        Vector2 _direction = Vector2.one;
        ConfigGeneral.ScreenSize _screenSpaceBall;

        void Start() => _screenSpaceBall = MainGameManager.init.GetScreenSpace(this.GetComponentInParent<Canvas>(), this.GetComponent<RectTransform>().rect.width / 2);

        void OnTriggerEnter2D(Collider2D other)
        {
            Vector2 lhs, rhs;
            lhs = other.transform.TransformDirection(Vector2.right).normalized;
            rhs = (transform.position - other.transform.position).normalized;

            bool isHitSidePart = Vector2.Dot(lhs, rhs) < -.9f || Vector2.Dot(lhs, rhs) > .9f;
            
            switch (other.tag)
            {
                case ConfigGeneral.TAG_PLAYER:
                    other.GetComponent<BoxSlider>().BoxSliderHit();
                    ReverseDirection(isHitSidePart, true);
                    break;
                case ConfigGeneral.TAG_ENEMY:
                    other.GetComponent<Enemy>().EnemyHit();
                    ReverseDirection(isHitSidePart, !isHitSidePart);
                    break;
            }
        }

        void FixedUpdate() => this.GetComponent<RectTransform>().anchoredPosition += GetDirection(this.GetComponent<RectTransform>()) * _screenSpaceBall.xMax * _speed * Time.deltaTime;
        void ReverseDirection(bool isX, bool isY) => _direction = new Vector2((isX) ? -_direction.x : _direction.x, (isY) ? -_direction.y : _direction.y);

        public void SetBall(Level.GameLevel lvl) => _speed = lvl.ballSpeed;
        public Vector2 GetDirection(RectTransform obj) => _direction = new Vector2(
            obj.anchoredPosition.x > _screenSpaceBall.xMax || obj.anchoredPosition.x < _screenSpaceBall.xMin ? -_direction.x : _direction.x,
            obj.anchoredPosition.y > _screenSpaceBall.yMax || obj.anchoredPosition.y < _screenSpaceBall.yMin ? -_direction.y : _direction.y);
    }
}