﻿using UnityEngine;

namespace Slash.Pong.MainGame
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] GameObject _particleEFX;
        float _downSpeed, _delay = 4, _timer;
        Vector2 targetPos;

        void Update()
        {
            _timer = (_timer > 0) ? _timer - Time.deltaTime : _delay / _downSpeed;
            targetPos = (_timer <= 0) ? this.GetComponent<RectTransform>().anchoredPosition : targetPos;
            targetPos.y -= (_timer <= 0) ? this.GetComponent<RectTransform>().rect.height : 0;
            this.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(this.GetComponent<RectTransform>().anchoredPosition, targetPos, Time.deltaTime * 3f);
        }

        public void SetEnemy(Level.GameLevel lvl)
        {
            _downSpeed = lvl.enemySpeed;
            _timer = _delay / _downSpeed;
            targetPos = this.GetComponent<RectTransform>().anchoredPosition;
        }

        public void EnemyHit()
        {
            MainGameManager.init.UpdateScore();
            _particleEFX = ObjectPoolManager.init.GetObjectToSpawn(General.ConfigGeneral.OBJECTID_PARTICLE_SMOKE);
            _particleEFX.transform.position = transform.position;
            _particleEFX.SetActive(true);
            ObjectPoolManager.init.ReturnObject(gameObject);
        }
    }
}
