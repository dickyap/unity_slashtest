﻿using System.Collections.Generic;
using UnityEngine;
using Slash.Pong.General;

namespace Slash.Pong.MainGame
{
    public class ObjectPoolManager : MonoBehaviour
    {
        public static ObjectPoolManager init;

        [Header("Enemy")]
        [SerializeField] GameObject _enemyPrefab;
        [SerializeField] Transform _parentEnemyObjs;

        [Header("Particle EFX")]
        [SerializeField] GameObject _psSmokePrefab;
        [SerializeField] Transform _parentParticleObjs;

        Dictionary<string, List<GameObject>> _dictAllObjs = new Dictionary<string, List<GameObject>>();
        int _initObject = 10;


        void Awake() => init = this;
        void Start() => InitializeObject();

        void InitializeObject()
        {
            for (int i = 0; i < _initObject; i++)
            {
                GenerateObj(_dictAllObjs, ConfigGeneral.OBJECTID_ENEMY, _enemyPrefab, _parentEnemyObjs);
                GenerateObj(_dictAllObjs, ConfigGeneral.OBJECTID_PARTICLE_SMOKE, _psSmokePrefab, _parentParticleObjs);
            }
        }

        GameObject GenerateObj(Dictionary<string, List<GameObject>> targetDict, string key, GameObject obj, Transform targetParent)
        {
            if (!targetDict.ContainsKey(key))
                targetDict.Add(key, new List<GameObject>());

            GameObject tempObj = Instantiate(obj, targetParent);
            ReturnObject(tempObj);

            targetDict[key].Add(tempObj);
            return tempObj;
        }

        public void ReturnObject(GameObject objToReturn)
        {
            if (objToReturn.GetComponent<RectTransform>() != null)
                objToReturn.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            else
                objToReturn.transform.localPosition = Vector3.zero;

            objToReturn.transform.localScale = Vector3.one;
            objToReturn.SetActive(false);
        }

        public GameObject GetObjectToSpawn(string objID) => GetObject(objID);
        public RectTransform GetParentCanvasRTObj() => _parentEnemyObjs.parent.root.GetComponent<RectTransform>();

        GameObject GetObject(string objID)
        {
            foreach (var item in _dictAllObjs[objID])
                if (!item.activeSelf) return item;

            return GenerateObj(_dictAllObjs, objID, _dictAllObjs[objID][0], _dictAllObjs[objID][0].transform.parent);
        }
    }
}