﻿using UnityEngine;
using Slash.Pong.General;

namespace Slash.Pong.MainGame
{
    public class BoxSlider : MonoBehaviour
    {
        float _speed = 4f;
        ConfigGeneral.ScreenSize _screenSpaceBoxSlider;

        void Start() => _screenSpaceBoxSlider = MainGameManager.init.GetScreenSpace(this.GetComponentInParent<Canvas>(), this.GetComponent<RectTransform>().rect.width / 2);
        // public void SetBoxSlider(Level.GameLevel lvl) {}
        public void BoxSliderHit() => this.GetComponent<Animator>().SetTrigger(ConfigGeneral.ANIM_TTRIGGER_BOUNCE);
        public void MoveBoxSlider(float value)
        {
            Vector3 targetPos = this.GetComponent<RectTransform>().anchoredPosition;
            targetPos.x = Mathf.Clamp(targetPos.x + value * _screenSpaceBoxSlider.xMax * _speed * Time.deltaTime, _screenSpaceBoxSlider.xMin, _screenSpaceBoxSlider.xMax);
            transform.GetComponent<RectTransform>().anchoredPosition = targetPos;
        }
    }
}
