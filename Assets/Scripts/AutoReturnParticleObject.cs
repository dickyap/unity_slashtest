﻿using System.Collections;
using UnityEngine;

namespace Slash.Pong.General
{
    [RequireComponent(typeof(ParticleSystem))]
    public class AutoReturnParticleObject : MonoBehaviour
    {
        void OnEnable() => StartCoroutine(CheckIsRunning(this.GetComponent<ParticleSystem>()));
        IEnumerator CheckIsRunning(ParticleSystem thisPS)
        {
            while (true)
            {
                yield return new WaitForSeconds(.5f);
                if (!thisPS.IsAlive(true)) MainGame.ObjectPoolManager.init.ReturnObject(gameObject);
            }
        }
    }
}
