﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Slash.Pong.Level
{
    [System.Serializable]
    [CreateAssetMenu(fileName = "GameLevel", menuName = "Slash/New Level", order = 1)]
    public class GameLevel : ScriptableObject
    {   
        public string levelID, levelName;
        [Header("Textures")]
        public Sprite sprBall;
        public Sprite sprBoxSlider, sprEnemy;
        [Header("Level Settings")]
        public float enemySpeed;
        public float ballSpeed;
        public int scorePerHit;
    }
}